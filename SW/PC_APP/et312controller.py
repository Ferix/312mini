import sys
import fcntl
import et312const
import buttshock.et312
from time import sleep 

class et312_controller:

    __power_a = 0
    __power_b = 0
    __multi_adjust = 0
    __mode = et312const.BOX_MODE_LIST["Waves"];
    __connected = False
    __et312 = None


    def __init__(self, com_port):
        self.__com_port = com_port

    def set_com_port(self, com_port):
        self.__com_port = com_port 

    def connect(self):  
        if(self.__connected == False):     
            for _ in range(10):
                try:
                    self.__et312 = buttshock.et312.ET312SerialSync(self.__com_port)
                    if self.__et312.port.isOpen():
                        fcntl.flock(self.__et312.port.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
                    break
                except Exception as e:
                    print(e)
                    sleep(.2)

            try:
                self.__et312.perform_handshake()
                self.__connected = True
            except Exception as e:
                self.__connected = False
                
        return self.__connected


    def is_connected(self):
        return self.__connected


    def disconnect(self):
        if(self.__connected):
            self.set_defaults()
            self.__et312.reset_key()
            sleep(0.05)


    def set_defaults(self):
        if(self.__connected):
            sleep(0.05)
            self.__et312.write(et312const.REG15, [1])
            sleep(0.05)
            self.__et312.write(et312const.POWER_A, [0])
            sleep(0.05)
            self.__et312.write(et312const.POWER_B, [0])
            sleep(0.05)
            self.__et312.write(et312const.BOX_MODES, [et312const.BOX_MODE_LIST["Waves"]]) 
            self.__et312.write(et312const.BOX_CMD0, [et312const.EXIT_MENU])
            self.__et312.write(et312const.BOX_CMD1, [et312const.NEW_MENU])
            sleep(0.05)
            self.__et312.write(et312const.MA_VALUE, [0])
            sleep(0.05)

    def set_power_a(self, new_value):
        if(self.__connected):
            if(self.__power_a != new_value):
                self.__power_a = new_value
                self.__et312.write(et312const.POWER_A, [ int(self.__power_a*2.4) ])
                sleep(0.05)
                #check
                if(self.__et312.read(et312const.POWER_A) != int(self.__power_a*2.4)):
                    print("uhoh")
                

    def set_power_b(self, new_value):
        if(self.__connected):
            if(self.__power_b != new_value):
                self.__power_b = new_value
                self.__et312.write(et312const.POWER_B, [ int(self.__power_b*2.4) ])
                sleep(0.05)
                #check
                if(self.__et312.read(et312const.POWER_B) != int(self.__power_b*2.4)):
                    print("uhoh")


    def set_multi_adjust(self, new_value):
        if(self.__connected):
            if(self.__multi_adjust != new_value):
                self.__multi_adjust = new_value
                self.__et312.write(et312const.MA_VALUE, [ int(self.__multi_adjust*2.4) ])
                sleep(0.05)
                #check
                if(self.__et312.read(et312const.MA_VALUE) != int(self.__multi_adjust*2.4)):
                    print("uhoh")


    def increase_mode(self):
        if(self.__connected):
            self.__mode = self.__mode + 1
            if(self.__mode > et312const.BOX_MODE_LAST):
                self.__mode = et312const.BOX_MODE_FIRST
            self.__et312.write(et312const.BOX_MODES, [self.__mode])
            self.__et312.write(et312const.BOX_CMD0, [et312const.EXIT_MENU])
            self.__et312.write(et312const.BOX_CMD1, [et312const.NEW_MENU])
            sleep(0.05)
            #check
            if(self.__mode != self.__et312.read(et312const.BOX_MODES)):
                print("uhoh");
                              

    def decrease_mode(self):
        if(self.__connected):
            self.__mode = self.__mode - 1
            if(self.__mode < et312const.BOX_MODE_FIRST):
                self.__mode = et312const.BOX_MODE_LAST
            self.__et312.write(et312const.BOX_MODES, [self.__mode])
            self.__et312.write(et312const.BOX_CMD0, [et312const.EXIT_MENU])
            self.__et312.write(et312const.BOX_CMD1, [et312const.NEW_MENU])
            sleep(0.05)
            #check
            if(self.__mode != self.__et312.read(et312const.BOX_MODES)):
                print("uhoh");

    def get_power_a(self):
        return self.__power_a


    def get_power_b(self):
        return self.__power_b


    def get_multi_adjust(self):
        return self.__multi_adjust

    def get_mode(self):
        return self.__mode
