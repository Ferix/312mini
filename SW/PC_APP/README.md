### A small python based GUI that can  do the following:
1. connect to the ET312 (it scans for an FTDI converter with product set to ZapTheFops atm)
2. control Level A, B and Multi Adjust
3. control the modes

No menu structure build in at the moment. who knows.

![](./Screenshot.png)

### To run:
Made for python 3
Need to install via pip:
1. pyserial
2. buttshock
3. bimpy
