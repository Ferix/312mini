import sys
import fcntl
import bimpy
import serial.tools.list_ports
import et312controller
import et312const

et312 = None
ctx = bimpy.Context()

ctx.init(600, 300, "ET312 controller")

def format_display(power_a, power_b, mode):
    return ("A: " + str(power_a) + " B: " + str(power_b) + "\n" + "Mode: " + et312const.BOX_MODE_LIST_REVERSE[mode])

def getPort():
    ports = list( serial.tools.list_ports.comports() )

    portFound = None

    resultPorts = []
    descriptions = []
    for port in ports:
        if port.description.startswith( "ZapTheFops" ):
            portFound = port.name

    return portFound

def main():
    val_a = bimpy.Int()
    val_b = bimpy.Int()
    val_mul = bimpy.Int()
    display_text = bimpy.String()

    connectButton = "Connect"

    port = getPort()
    if(port == None):
        connectButton = "Search for ET312"
        display_text.value = "No ET312 found!"
    else:
        port = "/dev/" + port
        display_text.value = "ET312 found!\nNot connected"

    et312 = et312controller.et312_controller(port)
    
    while(not ctx.should_close()):
        with ctx:

            bimpy.begin("Zapzap", bimpy.Bool(True), 0)
            
            bimpy.begin_child("buttons", bimpy.Vec2(150, 140), False, 0)

            if bimpy.button(connectButton, bimpy.Vec2(128, 60)):
                if(connectButton == "Search for ET312"):
                    port = getPort()
                    if(port != None):
                        port = "/dev/" + port
                        display_text.value = "ET312 found!\nNot connected"
                        connectButton = "Connect"
                        et312.set_com_port(port)
                elif(connectButton == "Connect"): 
                    if(et312.connect()):
                        et312.set_defaults()
            
            if bimpy.button("Next\nMode", bimpy.Vec2(60, 60)):
                et312.increase_mode()
                    
            bimpy.same_line()

            if bimpy.button("Prev\nMode", bimpy.Vec2(60, 60)):
                et312.decrease_mode()

            bimpy.end_child()

            bimpy.same_line()
            
            bimpy.begin_child("bars", bimpy.Vec2(300, 150), False, 0)
            
            bimpy.push_item_width(200)    
            bimpy.slider_int("Level A", val_a, 0, 100)
            bimpy.slider_int("Level B", val_b, 0, 100)
            bimpy.slider_int("Multi Adjust", val_mul, 0, 100)
            bimpy.pop_item_width()

            et312.set_power_a(val_a.value)
            et312.set_power_b(val_b.value)
            et312.set_multi_adjust(val_mul.value)

            if(et312.is_connected()):
                display_text.value = format_display(et312.get_power_a(), et312.get_power_b(), et312.get_mode())

            bimpy.input_text_multiline("##display", display_text, 100, bimpy.Vec2(200, 55), int(bimpy.InputTextFlags.ReadOnly))

            bimpy.end_child()

            bimpy.end()
            

    et312.disconnect()


if __name__ == '__main__':
    main()
