POWER_A = 0x4064
POWER_B = 0x4065
REG15 = 0x400f
BOX_MODES = 0x407b
MA_VALUE = 0x420D
EXIT_MENU = 0x04
NEW_MENU = 0x12
BOX_CMD0 = 0x4070
BOX_CMD1 = 0x4071

BOX_MODE_LIST_REVERSE = {0x76:"Waves", 0x77:"Stroke", 0x78:"Climb", 0x79:"Combo", 0x7a:"Intense",0x7b:"Rhythm",
             0x7c:"Audio1",0x7d:"Audio2", 0x7e:"Audio3", 0x80:"Random1", 0x81:"Random2", 0x82:"Toggle",
             0x83:"Orgasm",0x84:"Torment",0x85:"Phase1",0x86:"Phase2",0x87:"Phase3",
             0x88:"User1",0x89:"User2",0x8a:"User3",0x8b:"User4",0x8c:"User5",0:"None", 0x7f:"Split"}
    
BOX_MODE_LIST = {"Waves":0x76, "Stroke":0x77, "Climb":0x78, "Combo":0x79, "Intense":0x7a, "Rhythm":0x7b,
          "Audio1":0x7c,"Audio2":0x7d, "Audio3":0x7e, "Random1":0x80, "Random2":0x81, "Toggle":0x82,
          "Orgasm":0x83,"Torment":0x84,"Phase1":0x85,"Phase2":0x86,"Phase3":0x87,
          "User1":0x88,"User2":0x89,"User3":0x8a,"User4":0x8b,"User5":0x8c,"None":0, "Split":0x7f}

BOX_MODE_FIRST = 0x76
BOX_MODE_LAST = 0x8c
