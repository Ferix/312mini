

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.

info: https://github.com/buttshock/buttshock-protocol-docs/blob/master/doc/et312-protocol.org
"""
import sys
import fcntl
import et312const
import argparse
from time import sleep
import logging
import buttshock.et312
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


et312 = None
code = "zapthefops"
allowedchats = [0]

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

def checkcode(id):
    if (id in allowedchats):
        return True
    else:
        return False

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Zap the fops: \n \
        Send /powerinc to increase the power by 5% \n \
        Send /powerdec to decrease the power by 5% \n \
        Send /getpower to get the current set power \n \
        Send /getMA to get the MA level \n \
        Send /MAinc to increase the MA level by 5% \n \
        Send /MAdec to decrease the MA level by 5% \n \
        Send /getmode to get the current set mode \n \
        Send /setmode to set a mode \n \
        Send /addcode to add an access code '  )


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', bot, update.error)
    
def powerinc(update, context):
    if(checkcode(update.message.chat_id)):
      try:
          due = (context.args[0])
          if due == 'A':
              currentPower = et312.read(et312const.POWER_A)
              currentPower = currentPower + 12
              if(currentPower >= 240):
                  currentPower = 240
              
              et312.write(et312const.POWER_A, [currentPower])
              sleep(0.05)
              currentPower = et312.read(et312const.POWER_A)
              currentPower = (currentPower*100)/240
              getlevels = "Level A power set to: " + str(currentPower) + " %"
              update.message.reply_text(getlevels)
          elif due == 'B':
              currentPower = et312.read(et312const.POWER_B)
              currentPower = currentPower + 12
              if(currentPower >= 240):
                  currentPower = 240
              
              et312.write(et312const.POWER_B, [currentPower])
              sleep(0.05)
              currentPower = et312.read(et312const.POWER_B)
              currentPower = (currentPower*100)/240
              getlevels = "Level B power set to: " + str(currentPower) + " %"
              update.message.reply_text(getlevels)
          else:
              update.message.reply_text('Please select channel A or B!')
              return
      except (IndexError, ValueError):
          update.message.reply_text('Usage: /powerinc <channel>')
    else:
        update.message.reply_text('No valid code for this chat!')
    
    
    
def powerdec(update, context):
    if(checkcode(update.message.chat_id)):
        try:
            due = (context.args[0])
            if due == 'A':
                currentPower = et312.read(et312const.POWER_A)
                currentPower = currentPower - 12
                if(currentPower <= 0):
                    currentPower = 0
                
                et312.write(et312const.POWER_A, [currentPower])
                sleep(0.05)
                currentPower = et312.read(et312const.POWER_A)
                currentPower = (currentPower*100)/240
                getlevels = "Level A power set to: " + str(currentPower) + " %"
                update.message.reply_text(getlevels)
            elif due == 'B':
                currentPower = et312.read(et312const.POWER_B)
                currentPower = currentPower - 12
                if(currentPower <= 0):
                    currentPower = 0
                
                et312.write(et312const.POWER_B, [currentPower])
                sleep(0.05)
                currentPower = et312.read(et312const.POWER_B)
                currentPower = (currentPower*100)/240
                getlevels = "Level B power set to: " + str(currentPower) + " %"
                update.message.reply_text(getlevels)
            else:
                update.message.reply_text('Please select channel A or B!')
                return
        except (IndexError, ValueError):
            update.message.reply_text('Usage: /powerdec <channel>')
    else:
        update.message.reply_text('No valid code for this chat!')    
      
    
def getpower(update, context):
    levelApower = et312.read(et312const.POWER_A)
    levelApower = (levelApower*100)/240
    levelBpower = et312.read(et312const.POWER_B)
    levelBpower = (levelBpower*100)/240
    getlevels = "Level A power " + str(levelApower)  + " %, Level B power: "+ str(levelBpower) + " %"
    update.message.reply_text(getlevels)


def getMA(update, context):
    levelMA = et312.read(et312const.MA_VALUE)
    levelMA = (levelMA*100) / 240
    getMA = "MA set to: " + str(levelMA) + " %"
    update.message.reply_text(getMA)

def MAinc(update, context):
    if(checkcode(update.message.chat_id)):
        levelMA = et312.read(et312const.MA_VALUE)
        levelMA = levelMA + 12
        if (levelMA >= 240):
            levelMA = 240
            
        et312.write(et312const.MA_VALUE, [levelMA])
        sleep(0.05)
        levelMA = et312.read(et312const.MA_VALUE)
        levelMA = (levelMA*100) / 240
        getMA = "MA set to: " + str(levelMA) + " %"
        update.message.reply_text(getMA)
    else:
        update.message.reply_text('No valid code for this chat!')  
        
def MAdec(update, context):
    if(checkcode(update.message.chat_id)):
        levelMA = et312.read(et312const.MA_VALUE)
        levelMA = levelMA - 12
        if (levelMA <= 0):
            levelMA = 0
            
        et312.write(et312const.MA_VALUE, [levelMA])
        sleep(0.05)
        levelMA = et312.read(et312const.MA_VALUE)
        levelMA = (levelMA*100) / 240
        getMA = "MA set to: " + str(levelMA) + " %"
        update.message.reply_text(getMA)
    else:
        update.message.reply_text('No valid code for this chat!')  

def getmode(update, context):
    modes = {0x76:"Waves", 0x77:"Stroke", 0x78:"Climb", 0x79:"Combo", 0x7a:"Intense", 0x7b:"Rhythm",
             0x7c:"Audio1",0x7d:"Audio2", 0x7e:"Audio3", 0x80:"Random1", 0x81:"Random2", 0x82:"Toggle",
             0x83:"Orgasm",0x84:"Torment",0x85:"Phase1",0x86:"Phase2",0x87:"Phase3",
             0x88:"User1",0x89:"User2",0x8a:"User3",0x8b:"User4",0x8c:"User5",0:"None", 0x7f:"Split"}
    currentmode =et312.read(et312const.BOX_MODES)
    getmode = "Current Mode: " + modes[currentmode]
    update.message.reply_text(getmode)

def setmode(update, context):
    modesrev = {0x76:"Waves", 0x77:"Stroke", 0x78:"Climb", 0x79:"Combo", 0x7a:"Intense", 0x7b:"Rhythm",
             0x7c:"Audio1",0x7d:"Audio2", 0x7e:"Audio3", 0x80:"Random1", 0x81:"Random2", 0x82:"Toggle",
             0x83:"Orgasm",0x84:"Torment",0x85:"Phase1",0x86:"Phase2",0x87:"Phase3",
             0x88:"User1",0x89:"User2",0x8a:"User3",0x8b:"User4",0x8c:"User5",0:"None", 0x7f:"Split"}
    
    modes = {"Waves":0x76, "Stroke":0x77, "Climb":0x78, "Combo":0x79, "Intense":0x7a, "Rhythm":0x7b,
             "Audio1":0x7c,"Audio2":0x7d, "Audio3":0x7e, "Random1":0x80, "Random2":0x81, "Toggle":0x82,
             "Orgasm":0x83,"Torment":0x84,"Phase1":0x85,"Phase2":0x86,"Phase3":0x87,
             "User1":0x88,"User2":0x89,"User3":0x8a,"User4":0x8b,"User5":0x8c,"None":0, "Split":0x7f}
    if(checkcode(update.message.chat_id)):
        try:
            mode = (context.args[0])
            if mode in modes:
                et312.write(et312const.BOX_MODES, [modes[mode]])
                et312.write(et312const.BOX_CMD0, [et312const.EXIT_MENU])
                et312.write(et312const.BOX_CMD1, [et312const.NEW_MENU])
                sleep(0.05)
                currentmode =et312.read(et312const.BOX_MODES)
                getmode = "Current Mode: " + modesrev[currentmode]
                update.message.reply_text(getmode)
            else:
                update.message.reply_text(mode + " is not a valid mode, valid modes are: Waves, Stroke, Climb, Combo, Intense, Rhythm \
    Random1, Random2, Toggle, Orgasm, Torment, Split")
        except (IndexError, ValueError):
            update.message.reply_text('Usage: /setmode <mode>, valid modes are: Waves, Stroke, Climb, Combo, Intense, Rhythm \
    Random1, Random2, Toggle, Orgasm, Torment, Split')
    else:
        update.message.reply_text('No valid code for this chat!')

def addcode(update, context):
    chat_id = update.message.chat_id
    try:
        codetotry = context.args[0]
        if (codetotry == code):
            update.message.reply_text("Code OK");
            allowedchats.append(chat_id);
        else:
            update.message.reply_text("Code not OK");
            print(code)
    except (IndexError, ValueError):
        update.message.reply_text('Usage: /addcode <code>')
        
        
        
def main():
    global et312
    global code
    
    powerlevels = {1:"Low (1)",2:"Normal (2)",3:"High (3)"}

    parser = argparse.ArgumentParser()

    parser.add_argument("-p","--port",dest="port",help="Port for ET312 (default /dev/ttyUSB0)")    
    parser.add_argument("-c","--code",dest="code",help="Code for ET312")    
    args = parser.parse_args()
    
    if (args.code):
        code = args.code
        print(code)
        
    port = "/dev/ttyUSB0"  # lazy default
    if (args.port):
        port = args.port

    # Lock the serial port while we use it, wait a few seconds
    connected = False
    for _ in range(10):
        try:
            et312 = buttshock.et312.ET312SerialSync(port)
            if et312.port.isOpen():
                fcntl.flock(et312.port.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
                connected = True
            break
        except Exception as e:
            print(e)
            sleep(.2)

    if (not connected):
        print ("Failed")
        return

    try:
        et312.perform_handshake()
        print ("[+] connected")
    except Exception as e:
        print(e)
    reg = et312.read(et312const.REG15)
    print(reg)
    reg = reg + 1;
    sleep(0.05)
    et312.write(et312const.REG15, [1])
    sleep(0.05)
    reg = et312.read(et312const.REG15)
    et312.write(et312const.POWER_A, [0])
    sleep(0.05)
    et312.write(et312const.POWER_B, [0])
    sleep(0.05)
    et312.write(et312const.BOX_MODES, [0x76])  #default to Waves
    et312.write(et312const.BOX_CMD0, [et312const.EXIT_MENU])
    et312.write(et312const.BOX_CMD1, [et312const.NEW_MENU])
    sleep(0.05)
    et312.write(et312const.MA_VALUE, [0])
    sleep(0.05)
    print(reg)
    
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("896516103:AAFRmV2RNhyhDRXmzkZKpOVmX6ycRvbA9_k", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("powerinc", powerinc,
                                  pass_args = True,
                                  pass_chat_data=True))
    dp.add_handler(CommandHandler("powerdec", powerdec))
    dp.add_handler(CommandHandler("getpower", getpower))
    dp.add_handler(CommandHandler("getMA", getMA))
    dp.add_handler(CommandHandler("MAinc", MAinc))
    dp.add_handler(CommandHandler("MAdec", MAdec))
    dp.add_handler(CommandHandler("getmode", getmode))
    dp.add_handler(CommandHandler("setmode", setmode,
                                  pass_args = True,
                                  pass_chat_data=True))
    dp.add_handler(CommandHandler("addcode", addcode,
                                  pass_args = True,
                                  pass_chat_data=True))


    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
    
    if (et312):
        print("[+] resetting key")
        et312.reset_key()  # reset cipher key so easy resync next time
        et312.close()


if __name__ == '__main__':
    main()
