EESchema Schematic File Version 4
LIBS:312mini-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 312mini-rescue:ATmega16-16AU-MCU_Microchip_ATmega U3
U 1 1 5C9C8805
P 3750 3750
AR Path="/5C9C8805" Ref="U3"  Part="1" 
AR Path="/5C9A93FA/5C9C8805" Ref="U3"  Part="1" 
F 0 "U3" H 3300 5700 50  0000 C CNN
F 1 "ATmega16-16AU" H 3300 1800 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 3750 3750 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2466.pdf" H 3750 3750 50  0001 C CNN
	1    3750 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_GND24_Small Y1
U 1 1 5C9C88F8
P 1850 2450
F 0 "Y1" H 1991 2496 50  0000 L CNN
F 1 "Crystal_GND24_Small" H 1991 2405 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm_HandSoldering" H 1850 2450 50  0001 C CNN
F 3 "~" H 1850 2450 50  0001 C CNN
	1    1850 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5C9C896F
P 1500 2900
F 0 "C8" H 1615 2946 50  0000 L CNN
F 1 "18p" H 1615 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1538 2750 50  0001 C CNN
F 3 "~" H 1500 2900 50  0001 C CNN
	1    1500 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5C9C899D
P 2100 2900
F 0 "C9" H 2215 2946 50  0000 L CNN
F 1 "18p" H 2215 2855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2138 2750 50  0001 C CNN
F 3 "~" H 2100 2900 50  0001 C CNN
	1    2100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2450 2100 2450
Wire Wire Line
	2100 2450 2100 2750
Wire Wire Line
	1750 2450 1500 2450
Wire Wire Line
	1500 2450 1500 2750
Wire Wire Line
	2100 2450 3150 2450
Connection ~ 2100 2450
Wire Wire Line
	3150 2250 1500 2250
Wire Wire Line
	1500 2250 1500 2450
Connection ~ 1500 2450
$Comp
L power:GND #PWR013
U 1 1 5C9C8B02
P 1500 3150
F 0 "#PWR013" H 1500 2900 50  0001 C CNN
F 1 "GND" H 1505 2977 50  0000 C CNN
F 2 "" H 1500 3150 50  0001 C CNN
F 3 "" H 1500 3150 50  0001 C CNN
	1    1500 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3150 1500 3050
$Comp
L power:GND #PWR014
U 1 1 5C9C8B6B
P 2100 3150
F 0 "#PWR014" H 2100 2900 50  0001 C CNN
F 1 "GND" H 2105 2977 50  0000 C CNN
F 2 "" H 2100 3150 50  0001 C CNN
F 3 "" H 2100 3150 50  0001 C CNN
	1    2100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3050 2100 3150
$Comp
L power:+5V #PWR015
U 1 1 5C9C8C25
P 3050 2650
F 0 "#PWR015" H 3050 2500 50  0001 C CNN
F 1 "+5V" V 3065 2778 50  0000 L CNN
F 2 "" H 3050 2650 50  0001 C CNN
F 3 "" H 3050 2650 50  0001 C CNN
	1    3050 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 2650 3150 2650
$Comp
L power:+5V #PWR018
U 1 1 5C9C8CB2
P 3750 1650
F 0 "#PWR018" H 3750 1500 50  0001 C CNN
F 1 "+5V" H 3765 1823 50  0000 C CNN
F 2 "" H 3750 1650 50  0001 C CNN
F 3 "" H 3750 1650 50  0001 C CNN
	1    3750 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1650 3750 1700
Wire Wire Line
	3750 1700 3850 1700
Wire Wire Line
	3850 1700 3850 1750
Connection ~ 3750 1700
Wire Wire Line
	3750 1700 3750 1750
$Comp
L power:GND #PWR019
U 1 1 5C9C8F0E
P 3750 5800
F 0 "#PWR019" H 3750 5550 50  0001 C CNN
F 1 "GND" H 3755 5627 50  0000 C CNN
F 2 "" H 3750 5800 50  0001 C CNN
F 3 "" H 3750 5800 50  0001 C CNN
	1    3750 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5800 3750 5750
$Comp
L Device:C C10
U 1 1 5C9C91D0
P 3350 1150
F 0 "C10" H 3465 1196 50  0000 L CNN
F 1 "100n" H 3465 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3388 1000 50  0001 C CNN
F 3 "~" H 3350 1150 50  0001 C CNN
	1    3350 1150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR016
U 1 1 5C9C9267
P 3350 950
F 0 "#PWR016" H 3350 800 50  0001 C CNN
F 1 "+5V" H 3365 1123 50  0000 C CNN
F 2 "" H 3350 950 50  0001 C CNN
F 3 "" H 3350 950 50  0001 C CNN
	1    3350 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5C9C9291
P 3350 1350
F 0 "#PWR017" H 3350 1100 50  0001 C CNN
F 1 "GND" H 3355 1177 50  0000 C CNN
F 2 "" H 3350 1350 50  0001 C CNN
F 3 "" H 3350 1350 50  0001 C CNN
	1    3350 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1350 3350 1300
Wire Wire Line
	3350 1000 3350 950 
$Comp
L Device:C C11
U 1 1 5C9C94E7
P 4150 1200
F 0 "C11" H 4265 1246 50  0000 L CNN
F 1 "100n" H 4265 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4188 1050 50  0001 C CNN
F 3 "~" H 4150 1200 50  0001 C CNN
	1    4150 1200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR020
U 1 1 5C9C94EE
P 4150 1000
F 0 "#PWR020" H 4150 850 50  0001 C CNN
F 1 "+5V" H 4165 1173 50  0000 C CNN
F 2 "" H 4150 1000 50  0001 C CNN
F 3 "" H 4150 1000 50  0001 C CNN
	1    4150 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5C9C94F4
P 4150 1400
F 0 "#PWR021" H 4150 1150 50  0001 C CNN
F 1 "GND" H 4155 1227 50  0000 C CNN
F 2 "" H 4150 1400 50  0001 C CNN
F 3 "" H 4150 1400 50  0001 C CNN
	1    4150 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1400 4150 1350
Wire Wire Line
	4150 1050 4150 1000
Text HLabel 4650 4850 2    50   Input ~ 0
UC_TXD
Text HLabel 4650 4750 2    50   Input ~ 0
UC_RXD
Text HLabel 1200 1700 0    50   Input ~ 0
USB_VIN
Wire Wire Line
	1200 1700 3750 1700
Text HLabel 4650 3150 2    50   Input ~ 0
CHA_FET1
Text HLabel 4650 3250 2    50   Input ~ 0
CHA_FET2
Text HLabel 9350 4600 2    50   Input ~ 0
CHA_VSET
Text HLabel 4650 2950 2    50   Input ~ 0
CHB_FET1
Text HLabel 4650 3050 2    50   Input ~ 0
CHB_FET2
Text HLabel 9350 4300 2    50   Input ~ 0
CHB_VSET
Wire Wire Line
	4650 2950 4350 2950
Wire Wire Line
	4350 3050 4650 3050
Wire Wire Line
	4650 3150 4350 3150
Wire Wire Line
	4350 3250 4650 3250
Wire Wire Line
	4650 4750 4350 4750
Wire Wire Line
	4350 4850 4650 4850
$Comp
L Connector:AVR-ISP-6 J4
U 1 1 5C9D09EE
P 1500 6600
F 0 "J4" H 1220 6696 50  0000 R CNN
F 1 "AVR-ISP-6" H 1220 6605 50  0000 R CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Vertical" V 1250 6650 50  0001 C CNN
F 3 " ~" H 225 6050 50  0001 C CNN
	1    1500 6600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5C9D0C9D
P 1400 7100
F 0 "#PWR012" H 1400 6850 50  0001 C CNN
F 1 "GND" H 1405 6927 50  0000 C CNN
F 2 "" H 1400 7100 50  0001 C CNN
F 3 "" H 1400 7100 50  0001 C CNN
	1    1400 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 7100 1400 7000
$Comp
L power:+5V #PWR011
U 1 1 5C9D0F41
P 1400 6000
F 0 "#PWR011" H 1400 5850 50  0001 C CNN
F 1 "+5V" H 1415 6173 50  0000 C CNN
F 2 "" H 1400 6000 50  0001 C CNN
F 3 "" H 1400 6000 50  0001 C CNN
	1    1400 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6000 1400 6100
Text GLabel 2100 6400 2    50   Input ~ 0
MISO
Text GLabel 2100 6500 2    50   Input ~ 0
MOSI
Text GLabel 2100 6600 2    50   Input ~ 0
SCK
Text GLabel 2100 6700 2    50   Input ~ 0
RST
Wire Wire Line
	2100 6700 1900 6700
Wire Wire Line
	1900 6600 2100 6600
Wire Wire Line
	2100 6500 1900 6500
Wire Wire Line
	1900 6400 2100 6400
Text GLabel 4650 3550 2    50   Input ~ 0
MISO
Text GLabel 4650 3450 2    50   Input ~ 0
MOSI
Text GLabel 4650 3650 2    50   Input ~ 0
SCK
Text GLabel 3050 2050 0    50   Input ~ 0
RST
Wire Wire Line
	3050 2050 3150 2050
Wire Wire Line
	4650 3450 4350 3450
Wire Wire Line
	4350 3550 4650 3550
Wire Wire Line
	4650 3650 4350 3650
$Comp
L 312mini-rescue:LTC1661-riklib IC1
U 1 1 5C9D6A82
P 8350 4450
F 0 "IC1" H 8400 4825 50  0000 C CNN
F 1 "LTC1661" H 8400 4734 50  0000 C CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 8250 4500 50  0001 C CNN
F 3 "" H 8250 4500 50  0001 C CNN
	1    8350 4450
	1    0    0    -1  
$EndComp
Text GLabel 7650 4300 0    50   Input ~ 0
CS
Text GLabel 4650 5150 2    50   Input ~ 0
CS
Wire Wire Line
	4650 5150 4350 5150
Text GLabel 7650 4400 0    50   Input ~ 0
SCK
Text GLabel 7650 4500 0    50   Input ~ 0
MOSI
Wire Wire Line
	7650 4500 7850 4500
Wire Wire Line
	7850 4400 7650 4400
Wire Wire Line
	7650 4300 7850 4300
$Comp
L power:+5V #PWR027
U 1 1 5C9D82E5
P 7500 4600
F 0 "#PWR027" H 7500 4450 50  0001 C CNN
F 1 "+5V" H 7515 4773 50  0000 C CNN
F 2 "" H 7500 4600 50  0001 C CNN
F 3 "" H 7500 4600 50  0001 C CNN
	1    7500 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 4600 7850 4600
$Comp
L power:+5V #PWR030
U 1 1 5C9D8A12
P 9150 4150
F 0 "#PWR030" H 9150 4000 50  0001 C CNN
F 1 "+5V" H 9165 4323 50  0000 C CNN
F 2 "" H 9150 4150 50  0001 C CNN
F 3 "" H 9150 4150 50  0001 C CNN
	1    9150 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4500 9150 4500
Wire Wire Line
	9150 4500 9150 4150
$Comp
L power:GND #PWR031
U 1 1 5C9D9148
P 9250 4700
F 0 "#PWR031" H 9250 4450 50  0001 C CNN
F 1 "GND" H 9255 4527 50  0000 C CNN
F 2 "" H 9250 4700 50  0001 C CNN
F 3 "" H 9250 4700 50  0001 C CNN
	1    9250 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4400 9250 4400
Wire Wire Line
	9250 4400 9250 4700
$Comp
L Device:C C12
U 1 1 5C9D9A08
P 8450 5050
F 0 "C12" H 8565 5096 50  0000 L CNN
F 1 "100n" H 8565 5005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8488 4900 50  0001 C CNN
F 3 "~" H 8450 5050 50  0001 C CNN
	1    8450 5050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR028
U 1 1 5C9D9A0F
P 8450 4850
F 0 "#PWR028" H 8450 4700 50  0001 C CNN
F 1 "+5V" H 8465 5023 50  0000 C CNN
F 2 "" H 8450 4850 50  0001 C CNN
F 3 "" H 8450 4850 50  0001 C CNN
	1    8450 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5C9D9A15
P 8450 5250
F 0 "#PWR029" H 8450 5000 50  0001 C CNN
F 1 "GND" H 8455 5077 50  0000 C CNN
F 2 "" H 8450 5250 50  0001 C CNN
F 3 "" H 8450 5250 50  0001 C CNN
	1    8450 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 5250 8450 5200
Wire Wire Line
	8450 4900 8450 4850
Wire Wire Line
	9350 4300 8950 4300
Wire Wire Line
	9350 4600 8950 4600
Text HLabel 5550 2050 2    50   Input ~ 0
I_MES
Wire Wire Line
	5550 2050 5300 2050
$Comp
L Device:R R12
U 1 1 5CD9E2B7
P 5600 2550
F 0 "R12" V 5393 2550 50  0000 C CNN
F 1 "10K" V 5484 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5530 2550 50  0001 C CNN
F 3 "~" H 5600 2550 50  0001 C CNN
	1    5600 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5CD9E41B
P 5600 2450
F 0 "R11" V 5393 2450 50  0000 C CNN
F 1 "10K" V 5484 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5530 2450 50  0001 C CNN
F 3 "~" H 5600 2450 50  0001 C CNN
	1    5600 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5CD9E457
P 5600 2150
F 0 "R10" V 5393 2150 50  0000 C CNN
F 1 "10K" V 5484 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5530 2150 50  0001 C CNN
F 3 "~" H 5600 2150 50  0001 C CNN
	1    5600 2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5CD9E48F
P 5600 2650
F 0 "R13" V 5393 2650 50  0000 C CNN
F 1 "10K" V 5484 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5530 2650 50  0001 C CNN
F 3 "~" H 5600 2650 50  0001 C CNN
	1    5600 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5CD9E4D7
P 5600 2750
F 0 "R14" V 5393 2750 50  0000 C CNN
F 1 "10K" V 5484 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5530 2750 50  0001 C CNN
F 3 "~" H 5600 2750 50  0001 C CNN
	1    5600 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 2750 5300 2750
Wire Wire Line
	4350 2650 5300 2650
Wire Wire Line
	5450 2550 5300 2550
Wire Wire Line
	4350 2450 5300 2450
Wire Wire Line
	4350 2150 5300 2150
$Comp
L power:GND #PWR023
U 1 1 5CDA3B5E
P 6150 2850
F 0 "#PWR023" H 6150 2600 50  0001 C CNN
F 1 "GND" H 6155 2677 50  0000 C CNN
F 2 "" H 6150 2850 50  0001 C CNN
F 3 "" H 6150 2850 50  0001 C CNN
	1    6150 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2850 6150 2800
Wire Wire Line
	6150 2150 5750 2150
Wire Wire Line
	5750 2450 6150 2450
Connection ~ 6150 2450
Wire Wire Line
	6150 2450 6150 2150
Wire Wire Line
	5750 2550 6150 2550
Connection ~ 6150 2550
Wire Wire Line
	6150 2550 6150 2450
Wire Wire Line
	5750 2650 6150 2650
Connection ~ 6150 2650
Wire Wire Line
	6150 2650 6150 2550
Wire Wire Line
	5750 2750 6150 2750
Connection ~ 6150 2750
Wire Wire Line
	6150 2750 6150 2650
$Comp
L Device:R R19
U 1 1 5CDA847D
P 6450 2600
F 0 "R19" H 6380 2554 50  0000 R CNN
F 1 "5.6K" H 6380 2645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6380 2600 50  0001 C CNN
F 3 "~" H 6450 2600 50  0001 C CNN
	1    6450 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R18
U 1 1 5CDA84EB
P 6450 1950
F 0 "R18" H 6380 1904 50  0000 R CNN
F 1 "10K" H 6380 1995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6380 1950 50  0001 C CNN
F 3 "~" H 6450 1950 50  0001 C CNN
	1    6450 1950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R21
U 1 1 5CDA8535
P 6850 1950
F 0 "R21" H 6780 1904 50  0000 R CNN
F 1 "10K" H 6780 1995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6780 1950 50  0001 C CNN
F 3 "~" H 6850 1950 50  0001 C CNN
	1    6850 1950
	-1   0    0    1   
$EndComp
$Comp
L Device:R R22
U 1 1 5CDA85C8
P 6850 2600
F 0 "R22" H 6780 2554 50  0000 R CNN
F 1 "3.3K" H 6780 2645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6780 2600 50  0001 C CNN
F 3 "~" H 6850 2600 50  0001 C CNN
	1    6850 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 2750 6850 2800
Wire Wire Line
	6850 2800 6450 2800
Connection ~ 6150 2800
Wire Wire Line
	6150 2800 6150 2750
Wire Wire Line
	6450 2800 6450 2750
Connection ~ 6450 2800
Wire Wire Line
	6450 2800 6150 2800
Wire Wire Line
	6450 2450 6450 2350
Wire Wire Line
	6850 2100 6850 2250
Wire Wire Line
	4350 2350 5300 2350
Connection ~ 6450 2350
Wire Wire Line
	6450 2350 6450 2100
Wire Wire Line
	4350 2250 5300 2250
Connection ~ 6850 2250
Wire Wire Line
	6850 2250 6850 2450
$Comp
L power:+9V #PWR024
U 1 1 5CDAFF1D
P 6450 1700
F 0 "#PWR024" H 6450 1550 50  0001 C CNN
F 1 "+9V" H 6465 1873 50  0000 C CNN
F 2 "" H 6450 1700 50  0001 C CNN
F 3 "" H 6450 1700 50  0001 C CNN
	1    6450 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+9VA #PWR026
U 1 1 5CDAFFC9
P 6850 1700
F 0 "#PWR026" H 6850 1575 50  0001 C CNN
F 1 "+9VA" H 6865 1873 50  0000 C CNN
F 2 "" H 6850 1700 50  0001 C CNN
F 3 "" H 6850 1700 50  0001 C CNN
	1    6850 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1700 6850 1800
Wire Wire Line
	6450 1700 6450 1800
$Comp
L Device:LED D5
U 1 1 5CDB3242
P 5600 5850
F 0 "D5" V 5545 5928 50  0000 L CNN
F 1 "LED" V 5636 5928 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm" H 5600 5850 50  0001 C CNN
F 3 "~" H 5600 5850 50  0001 C CNN
	1    5600 5850
	0    1    1    0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5CDB4AA6
P 5200 5850
F 0 "D4" V 5145 5928 50  0000 L CNN
F 1 "LED" V 5236 5928 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm" H 5200 5850 50  0001 C CNN
F 3 "~" H 5200 5850 50  0001 C CNN
	1    5200 5850
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5CDB4B26
P 5200 6250
F 0 "R15" H 5130 6204 50  0000 R CNN
F 1 "1K" H 5130 6295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5130 6250 50  0001 C CNN
F 3 "~" H 5200 6250 50  0001 C CNN
	1    5200 6250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R20
U 1 1 5CDB4C00
P 5600 6250
F 0 "R20" H 5530 6204 50  0000 R CNN
F 1 "1K" H 5530 6295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5530 6250 50  0001 C CNN
F 3 "~" H 5600 6250 50  0001 C CNN
	1    5600 6250
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR022
U 1 1 5CDB4DEC
P 5200 6550
F 0 "#PWR022" H 5200 6400 50  0001 C CNN
F 1 "+5V" H 5215 6723 50  0000 C CNN
F 2 "" H 5200 6550 50  0001 C CNN
F 3 "" H 5200 6550 50  0001 C CNN
	1    5200 6550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5200 6550 5200 6500
Wire Wire Line
	5600 6500 5600 6400
Wire Wire Line
	5200 6500 5600 6500
Connection ~ 5200 6500
Wire Wire Line
	5200 6500 5200 6400
Wire Wire Line
	5200 6000 5200 6100
Wire Wire Line
	5600 6100 5600 6000
Wire Wire Line
	5600 5700 5600 5350
Wire Wire Line
	5600 5350 4350 5350
Wire Wire Line
	5200 5700 5200 5250
Wire Wire Line
	5200 5250 4350 5250
Text Notes 5700 6000 0    50   ~ 0
Channel A\n
Text Notes 4750 6000 0    50   ~ 0
Channel B\n
$Comp
L Device:R R17
U 1 1 5CDC69C6
P 5250 5050
F 0 "R17" V 5043 5050 50  0000 C CNN
F 1 "10K" V 5134 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5180 5050 50  0001 C CNN
F 3 "~" H 5250 5050 50  0001 C CNN
	1    5250 5050
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 5CDC6BBE
P 5250 4950
F 0 "R16" V 5043 4950 50  0000 C CNN
F 1 "10K" V 5134 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5180 4950 50  0001 C CNN
F 3 "~" H 5250 4950 50  0001 C CNN
	1    5250 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 5050 4350 5050
Wire Wire Line
	4350 4950 5100 4950
$Comp
L power:GND #PWR025
U 1 1 5CDCAB09
P 5550 5150
F 0 "#PWR025" H 5550 4900 50  0001 C CNN
F 1 "GND" H 5555 4977 50  0000 C CNN
F 2 "" H 5550 5150 50  0001 C CNN
F 3 "" H 5550 5150 50  0001 C CNN
	1    5550 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 5150 5550 5050
Wire Wire Line
	5550 4950 5400 4950
Wire Wire Line
	5400 5050 5550 5050
Connection ~ 5550 5050
Wire Wire Line
	5550 5050 5550 4950
$Comp
L Connector_Generic:Conn_01x10 J5
U 1 1 5CDB7DC3
P 5100 2350
F 0 "J5" H 5020 2967 50  0000 C CNN
F 1 "Conn_01x10" H 5020 2876 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x10_P1.27mm_Vertical" H 5100 2350 50  0001 C CNN
F 3 "~" H 5100 2350 50  0001 C CNN
	1    5100 2350
	-1   0    0    -1  
$EndComp
Connection ~ 5300 2050
Wire Wire Line
	5300 2050 4350 2050
Connection ~ 5300 2150
Wire Wire Line
	5300 2150 5450 2150
Connection ~ 5300 2250
Wire Wire Line
	5300 2250 6850 2250
Connection ~ 5300 2350
Wire Wire Line
	5300 2350 6450 2350
Connection ~ 5300 2450
Wire Wire Line
	5300 2450 5450 2450
Connection ~ 5300 2550
Wire Wire Line
	5300 2550 4350 2550
Connection ~ 5300 2650
Wire Wire Line
	5300 2650 5450 2650
Connection ~ 5300 2750
Wire Wire Line
	5300 2750 4350 2750
$Comp
L power:GND #PWR060
U 1 1 5CDC11A4
P 5350 2900
F 0 "#PWR060" H 5350 2650 50  0001 C CNN
F 1 "GND" H 5355 2727 50  0000 C CNN
F 2 "" H 5350 2900 50  0001 C CNN
F 3 "" H 5350 2900 50  0001 C CNN
	1    5350 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2850 5350 2850
Wire Wire Line
	5350 2850 5350 2900
$Comp
L power:+5V #PWR061
U 1 1 5CDC6A7A
P 5400 1900
F 0 "#PWR061" H 5400 1750 50  0001 C CNN
F 1 "+5V" H 5415 2073 50  0000 C CNN
F 2 "" H 5400 1900 50  0001 C CNN
F 3 "" H 5400 1900 50  0001 C CNN
	1    5400 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 1950 5400 1950
Wire Wire Line
	5400 1950 5400 1900
$Comp
L Connector_Generic:Conn_01x10 J6
U 1 1 5CDCC758
P 5800 4150
F 0 "J6" H 5880 4142 50  0000 L CNN
F 1 "Conn_01x10" H 5880 4051 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x10_P1.27mm_Vertical" H 5800 4150 50  0001 C CNN
F 3 "~" H 5800 4150 50  0001 C CNN
	1    5800 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4550 5400 4550
Wire Wire Line
	4350 4450 5600 4450
Wire Wire Line
	5600 4350 4350 4350
Wire Wire Line
	4350 4250 5600 4250
Wire Wire Line
	5600 4150 4350 4150
Wire Wire Line
	4350 4050 5600 4050
Wire Wire Line
	5600 3950 4350 3950
Wire Wire Line
	5600 3850 4350 3850
Wire Wire Line
	5550 4650 5600 4650
$Comp
L power:+5V #PWR058
U 1 1 5CDECF79
P 5550 3750
F 0 "#PWR058" H 5550 3600 50  0001 C CNN
F 1 "+5V" H 5565 3923 50  0000 C CNN
F 2 "" H 5550 3750 50  0001 C CNN
F 3 "" H 5550 3750 50  0001 C CNN
	1    5550 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3750 5550 3750
$Comp
L Device:R R50
U 1 1 5D79BFB3
P 5400 4700
F 0 "R50" V 5193 4700 50  0000 C CNN
F 1 "10K" V 5284 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5330 4700 50  0001 C CNN
F 3 "~" H 5400 4700 50  0001 C CNN
	1    5400 4700
	-1   0    0    1   
$EndComp
Connection ~ 5400 4550
Wire Wire Line
	5400 4550 4350 4550
Wire Wire Line
	5550 4650 5550 4850
Connection ~ 5550 4950
Wire Wire Line
	5400 4850 5550 4850
Connection ~ 5550 4850
Wire Wire Line
	5550 4850 5550 4950
$EndSCHEMATC
